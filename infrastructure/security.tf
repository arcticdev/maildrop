# Security group for requests to the SMTP load balancer

resource "aws_security_group" "lb" {
  name = "maildrop-smtp-lb"
  description = "Access to the SMTP load balancer"
  vpc_id = "${aws_vpc.maildrop.id}"

  ingress {
    from_port = 25
    protocol = "tcp"
    to_port = 25
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Security group for the ECS cluster

resource "aws_security_group" "ec2" {
  name = "maildrop-ecs-ec2"
  description = "Access to the EC2 instances"
  vpc_id = "${aws_vpc.maildrop.id}"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["${aws_subnet.public.cidr_block}"]
  }

  ingress {
    from_port = 1024
    protocol = "tcp"
    to_port = 65535
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# Security group for the ECS tasks

resource "aws_security_group" "tasks" {
  name = "maildrop-ecs-tasks"
  description = "Access to the ECS containers"
  vpc_id = "${aws_vpc.maildrop.id}"

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["${aws_subnet.public.cidr_block}"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "lambda_security_group" {
  value = "${aws_security_group.ec2.id}"
}

# Security group for Redis

resource "aws_security_group" "redis" {
  name = "maildrop-redis"
  description = "Access to Redis"
  vpc_id = "${aws_vpc.maildrop.id}"

  ingress {
    from_port = 6379
    protocol = "tcp"
    to_port = 6379
    cidr_blocks = ["${aws_subnet.public.cidr_block}"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}